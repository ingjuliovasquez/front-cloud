import { createBrowserRouter } from "react-router-dom";
import App from "../App";
import NewOffice from "../pages/offices/NewOffice";
import AssigmentForm from "../pages/assignment/AssigmentForm";
import OfficeAssignment from "../pages/offices/OfficeAssignment";
import IsEmpty from "../components/common/IsEmpty";
import ApartmentIcon from '@mui/icons-material/Apartment';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: '/',
        element: <IsEmpty Icon={ApartmentIcon} size='50px' message='Seleccione un departamento para ver su designación de gastos o para realizar una asignación' />
      },
      {
        path: "list/:id",
        element: <OfficeAssignment />,
      },
      {
        path: "nuevo-departamento",
        element: <NewOffice />,
      },
      {
        path: 'nueva-asignacion/:id',
        element: <AssigmentForm />
      }
    ],
  },
]);

export default router;
