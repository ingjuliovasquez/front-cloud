import React from "react";
import { Link } from "react-router-dom";
import IsEmpty from '../../components/common/IsEmpty'

const Office = ({ offices }) => {
  return (
  <>
    <Link to="/">
      <h3 className="text-white text-center text-lg md:text-xl mt-3">
        Departamentos
      </h3>
    </Link>
    {offices?.length > 0 ? (
      <div className="container mx-auto grid md:grid-cols-3 gap-8 mt-5">
        {offices?.map((item, index) => (
          <div
            key={item.office}
            className="flex flex-col justify-center rounded-lg bg-white shadow-lg px-4 py-3 transition hover:shadow-xl hover:shadow-blue-700"
          >
            <Link to={`list/${index}`}>
              <p className="text-center text-blue-600 font-bold text-1xl mb-2">{item.office}</p>
            </Link>
            <div className="text-center font-medium mb-2 text-base">
              <p>Empleados: {item.employees?.length + 1}</p> 
              <p>Encargado: {item.manager.title}</p>
            </div>
            <Link className="flex justify-center" to={`nueva-asignacion/${index}`}>
              <button className="rounded-lg bg-blue-600 shadow-lg text-white p-2 mb-2 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-100 duration-300">
                Determinar asignación
              </button>
            </Link>
          </div>
        ))}
      </div>
    ) : (
      <IsEmpty message="Registre un nuevo departamento" />
    )}
  </>
)};

export default Office;
