import React from "react";
import ControllerInput from "../../components/common/ControllerInput";
import useOfficeForm from "../../hooks/office/useOfficeForm";

const NewOffice = ( {row = null}) => {
  const { control, handleSubmit, onSubmit } = useOfficeForm({row});
  return (
    <div className="flex justify-center shadow-lg bg-white px-8 py-6 w-4/5 md:w-1/2 mx-auto rounded-lg mb-10">
      <form onSubmit={handleSubmit(onSubmit)}>
        <h3 className="font-bold text-2xl uppercase text-center">
          Nuevo departamento
        </h3>
        <div className="mt-3">
          <ControllerInput
            control={control}
            name="office"
            label="Nombre del departamento"
          />
        </div>
        <button
          type="submit"
          className="rounded-lg mt-3 p-2 shadow text-white bg-blue-700"
        >
          Enviar
        </button>
      </form>
    </div>
  );
};

export default NewOffice;
