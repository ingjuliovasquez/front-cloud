import React from "react";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import useOfficeAssignment from "../../hooks/assigment/useOfficeAssignment";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import { ListItemIcon } from "@mui/material";

const OfficeAssignment = () => {
  const { office } = useOfficeAssignment();
  let total = 0 + office.manager.amount
  return (
    <div className="mx-auto w-4/5 md:w-1/2 rounded-lg shadow-lg bg-white py-2 mb-8 px-3">
      <h3 className="font-semibold text-center mt-5">
        Lista de asignación en el departamento {office.office}
      </h3>
      <p className="font-light ml-6 mt-3">Niveles:</p>
      <List
        sx={{ width: "100%", bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        <ListItemButton sx={{ pl: 3 }}>
          <ListItemIcon sx={{ minWidth: "25px" }}>
            <RadioButtonCheckedIcon fontSize="sm" />
          </ListItemIcon>
          <div className="flex justify-between w-full">
            <p className="font-semibold">{office.manager.title}</p> 
            <p>{`($${office.manager.amount.toFixed(2)})`}</p>
          </div>
        </ListItemButton>
        <List component="div" disablePadding>
          {office.employees.length > 0 &&
            office.employees.map((item, index) => {
              total += item.amount 
              return(
              <ListItemButton key={`list-${index}`} sx={{ pl: 6 }}>
                <ListItemIcon sx={{ minWidth: "25px" }}>
                    <RadioButtonUncheckedIcon fontSize="sm" />
                </ListItemIcon>
                <ListItemText primary={item.title}/> <span>{`($${item.amount.toFixed(2)})`}</span>
              </ListItemButton>
            )})}
        </List>
      </List>
      <p className="font-bold text-lg p-4 flex justify-end">
        Total: ${total.toFixed(2)}
      </p>
    </div>
  );
};

export default OfficeAssignment;
