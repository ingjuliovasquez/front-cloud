import React from "react";
import ControllerInput from "../../components/common/ControllerInput";
import { Fab } from "@mui/material";
import { Add } from "@mui/icons-material";
import { fieldRequired } from "../../utils/validations";

const AssignmentOfficeForm = ({control, offices, addOffice}) => {
  return (
    <>
      <div className="w-1/2">
        <ControllerInput
          name="totalAmount"
          type="number"
          control={control}
          label="* Monto total asignado"
          rules={fieldRequired}
        />
      </div>
      <div className="flex gap-2">
            <ControllerInput
              className="w-3/5"
              name="manager.title"
              control={control}
              label="* Jefe de depto"
              rules={fieldRequired}
            />
            <ControllerInput
              type="number"
              className="w-2/5"
              name="manager.amount"
              control={control}
              label="* Monto"
              rules={fieldRequired}
            />
          </div>
      <div className="flex gap-2">
        <div className="flex flex-col">
          {offices.map((_item, index) => (
            <div className="flex gap-2" key={`office-${index + 1}`}>
              <ControllerInput
                className="w-3/5"
                name={`office.${index}.officeName`}
                control={control}
                label="* Oficina"
                rules={fieldRequired}
              />
              <ControllerInput
                className="w-2/5"
                name={`office.${index}.officeAmount`}
                control={control}
                label="* Monto"
                rules={fieldRequired}
              />
            </div>
          ))}
        </div>
        {offices.length <= 3 && (
          <div className="mt-1">
            <Fab
              onClick={() => {
                return addOffice({ ["officeName"]: "" });
              }}
              size="small"
              color="primary"
              aria-label="add"
            >
              <Add />
            </Fab>
          </div>
        )}
      </div>
      <ControllerInput
        name="justification"
        control={control}
        label="* Justificación del gasto"
        rules={fieldRequired}
      />
    </>
  );
};

export default AssignmentOfficeForm;
