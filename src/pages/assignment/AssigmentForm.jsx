import React from "react";
import Form from "../../components/common/Form";
import ControllerInput from "../../components/common/ControllerInput";
import { Add } from "@mui/icons-material";
import { Fab } from "@mui/material";
import useAssignmentForm from "../../hooks/assigment/useAssignmentForm";
import RadioGroupController from "../../components/common/RadioGroupController";
import AssignmentOfficeForm from "./AssignmentOfficeForm";
import { fieldRequired } from "../../utils/validations";

const AssigmentForm = () => {
  const {
    control,
    watch,
    handleSubmit,
    onSubmit,
    errors,
    employees,
    addEmployee,
    offices,
    addOffice,
  } = useAssignmentForm();

  return (
    <Form onSubmit={handleSubmit(onSubmit)} errors={errors}>
      <h3 className="font-bold text-center text-lg mb-3">
        Asignación de gastos mensuales
      </h3>
      <RadioGroupController
        name="assignmentType"
        control={control}
        options={[
          { id: 1, name: "Empleados", value: 1 },
          { id: 2, name: "Departamentos", value: 2 },
        ]}
        label="Tipo de asignación"
        isHorizontal
        className="mb-3"
      />
      {watch("assignmentType") === 2 ? (
        <AssignmentOfficeForm
          control={control}
          offices={offices}
          addOffice={addOffice}
        />
      ) : (
        <>
          <div className="w-1/2">
            <ControllerInput
              name="totalAmount"
              type="number"
              control={control}
              label="* Monto total asignado"
              rules={fieldRequired}
            />
          </div>
          <div className="flex gap-2">
            <ControllerInput
              className="w-3/5"
              name="manager.title"
              control={control}
              label="* Jefe de depto"
              rules={fieldRequired}
            />
            <ControllerInput
              type="number"
              className="w-2/5"
              name="manager.amount"
              control={control}
              label="* Monto"
              rules={fieldRequired}
            />
          </div>
          <div className="flex gap-2">
            <div className="flex flex-col">
              {employees.map((_item, index) => (
                <div className="flex gap-2" key={`employee-${index + 1}`}>
                  <ControllerInput
                    className="w-3/5"
                    name={`employee.${index}.title`}
                    control={control}
                    label="* Tipo de empleado"
                    rules={fieldRequired}
                  />
                  <ControllerInput
                    className="w-2/5"
                    name={`employee.${index}.employeeAmount`}
                    control={control}
                    label="* Monto"
                    type="number"
                    rules={fieldRequired}
                  />
                </div>
              ))}
            </div>
            {employees.length <= 3 && (
              <div className="mt-1">
                <Fab
                  onClick={() => {
                    return addEmployee({ ["title"]: "" });
                  }}
                  size="small"
                  color="primary"
                  aria-label="add"
                >
                  <Add />
                </Fab>
              </div>
            )}
          </div>
          <ControllerInput
            name="justification"
            control={control}
            label="* Justificación del gasto"
            rules={fieldRequired}
          />
        </>
      )}
      <button
        type="submit"
        className="rounded-lg mt-3 p-2 shadow text-white bg-blue-700"
      >
        Enviar
      </button>
    </Form>
  );
};

export default AssigmentForm;
