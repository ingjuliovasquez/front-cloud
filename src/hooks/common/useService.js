

import { useSelect } from '@mui/base'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

export const useListService = ({
    store,
    listadoName = 'list',
  }) => {
    // const dispatch = useDispatch();
    const list = useSelector((state) => state[store][listadoName]);
  
    return {
      data: list || [],
      isLoading: isFetching
    };
  };

export default useListService