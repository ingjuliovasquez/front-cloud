import React from 'react'
import { useForm } from 'react-hook-form'

const DEFAULT_VALUES = {
    
}

const useOfficeForm = ({row}) => {
  
    const { control, handleSubmit, formState: { errors} } = useForm({
        defaultValues: row ?? DEFAULT_VALUES
    })

    const onSubmit = async (body) => {
        console.log(body)
    }

    return {
        control, handleSubmit, errors, onSubmit
    }
}

export default useOfficeForm