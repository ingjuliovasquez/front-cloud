import { useSelector } from "react-redux";

const useOffice = () => {
  const offices = useSelector(({assignments}) => assignments.offices)
  return {
    offices
  }

}

export default useOffice