import { useFieldArray, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addAssignmentService } from "../../redux/slice/assignmentSlice";
import { showToastError, showToastSuccess } from "../../utils/toas";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, useParams } from "react-router-dom";

const useAssignmentForm = () => {
  const navigate = useNavigate()
  const { id } = useParams();
  const officeField = useSelector(({ assignments }) => assignments.offices[id]);
  const DEFAULT_VALUES = {
    assignmentType: 1,
    employee: [{ title: "" }],
    office: [{ officeName: "" }],
    totalAmount: officeField.estimate && 0,
    manager: {
      title: '',
      amount: 0,
    },
  };

  const dispatch = useDispatch();
  const {
    control,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: DEFAULT_VALUES,
  });

  const { fields: employees, append: addEmployee } = useFieldArray({
    control,
    name: "employee",
  });

  const { fields: offices, append: addOffice } = useFieldArray({
    control,
    name: "office",
  });

  const onSubmit = async (data) => {

    const employees = []

    if (data.employee.length >= 2) {
      data.employee?.map(item => {
        const payload = {
          title: item.title,
          amount: Number(item.employeeAmount)
        }
        return employees.push(payload)
      })
    }
    const payload = {
      office: officeField.office,
      assignmentType: data.assignmentType === 2 ? "Oficina" : "Empleados",
      estimate: Number(data.totalAmount),
      employees: employees?.length ? employees : [],
      offices: data.office?.length ? data.office : [],
      manager: {
        title: data.manager.title,
        amount: Number(data.manager.amount),
      },
      justification: data.justification
    }
    const response = await dispatch(addAssignmentService(payload));
    if (response.status === 200) {
      showToastSuccess(response.message);
      navigate(`/list/${id}`)
    } else {
      showToastError("Ocurrio un error, revise la información ingresada");

    }
  };

  return {
    watch,
    control,
    handleSubmit,
    errors,
    employees,
    addEmployee,
    onSubmit,
    offices,
    addOffice,
  };
};

export default useAssignmentForm;
