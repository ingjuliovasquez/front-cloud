import React from 'react'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'

const useOfficeAssignment = () => {
  
    const {id} = useParams()
    const office = useSelector(({assignments}) => assignments.offices[id])
    return {
        office
    }

}

export default useOfficeAssignment