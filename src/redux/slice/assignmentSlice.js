import { createSlice } from "@reduxjs/toolkit";
import { updateRow } from "../../utils/map";

const initialState = {
  offices: [
    {
      assignmentType: '',
      employees: [{ title: "Subgerente", amount: 100 }],
      estimate: 1580.0,
      justification: '',
      manager: {
        title: "Gerente",
        amount: 100,
      },
      office: "Gerentes",
    },
    {
      assignmentType: '',
      employees: [
        { title: "Subgerencia", amount: 100 },
        { title: "Encargado de área", amount: 100 },
        { title: "Auxiliar", amount: 100 },
      ],
      estimate: 2560.0,
      justification: '',
      manager: {
        title: "Senior Developr",
        amount: 100,
      },
      office: "Desarrolladores",
    },
    {
      assignmentType: '',
      employees: [
        { title: "Subgerencia", amount: 100 },
        { title: "Empleado", amount: 100 },
      ],
      estimate: 8320.0,
      justification: '',
      manager: {
        title: "QA",
        amount: 100,
      },
      office: "Probadores de control",
    },
  ],
  isLoading: true,
};

export const assignmentSlice = createSlice({
  name: "assignments",
  initialState,
  reducers: {
    setAssignments: (state, { payload }) => {
      state.offices = payload;
    },
    addAssignment: (state, { payload }) => {
      state.offices = [payload, ...offices];
    },
    updateAssignment: (state, { payload }) => {
      state.offices = updateRow(state.offices, payload, "office");
    },
    setIsLoading: (state, { payload }) => {
      state.isLoading = payload;
    },
  },
});

export const { setAssignments, addAssignment, updateAssignment, setIsLoading } =
  assignmentSlice.actions;

export const addAssignmentService = (data) => (dispatch) => {
  try {
    dispatch(updateAssignment(data))
    return {
      status: 200,
      message: 'Transacción exitosa',
      data: data
    };
  } catch (error) {
    return {
      status: 401,
      message: 'Transacción fallida'
    };
  }
};

export default assignmentSlice.reducer;
