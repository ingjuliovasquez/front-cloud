import { configureStore } from "@reduxjs/toolkit";

import assignments from "./slice/assignmentSlice";

const store = configureStore({
  reducer: {
    assignments,
  },
});

export default store;
