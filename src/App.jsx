import { Suspense } from "react";
import { Outlet } from "react-router-dom";
import { LinearProgress } from "@mui/material";
import useOffice from "./hooks/office/useOffice";
import Header from './components/Header'
import Office from './pages/offices/Office'

function App() {

  const {offices} = useOffice()

  return (
    <>
      <div className="bg-blue-800 shadow-lg w-full px-12 py-8">
        <Header />
        <Office offices={offices} />
      </div>
      <div className="container mx-auto mt-[25px]">
        <Suspense fallback={<LinearProgress />}>
          <Outlet />
        </Suspense>
      </div>
    </>
  );
}

export default App;
