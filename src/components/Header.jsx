import React from "react";
import LibraryAddIcon from "@mui/icons-material/LibraryAdd";
import { Fab } from "@mui/material";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="relative">
      <h1 className="text-xl md:text-2xl uppercase text-white text-center font-bold">
        Control de asignaciones
      </h1>
      <div className="absolute -top-5 -right-10 md:top-0 md:-right-1 flex justify-center items-center cursor-pointer transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-100 duration-300">
        <Link to='/nuevo-departamento'>
          <Fab size="small" color="white" aria-label="Agregar Departamento">
            <LibraryAddIcon />
          </Fab>
        </Link>
      </div>
    </header>
  );
};

export default Header;
