import React from "react";
import { CircularProgress } from "@mui/material";

const Form = ({
  isLoading = false,
  onSubmit,
  children,
  errors = {},
  ...props
}) => {

  const isEmptyObject = (obj) =>
    obj &&
    Object.keys(obj).length === 0 &&
    Object.getPrototypeOf(obj) === Object.prototype;

  if (isLoading)
    return (
      <div className="py-5 text-center">
        <CircularProgress size={40} />
      </div>
    );

  return (
    <div className="w-4/5 md:w-1/2 bg-white rounded-lg shadow-lg mx-auto mt-3 mb-5 py-7 px-5">
      <form onSubmit={onSubmit} {...props}>
        {children}
      { !isEmptyObject(errors) && (
          <p className="text-center text-sm text-red-600 font-semibold mt-3">Por favor, revise los campos obligatorios (*) o asegurese de enviar 2 empleados como mínimo.</p>
        )}
      </form>
    </div>
  );
};

export default Form;
