import { FormLabel, InputLabel } from "@mui/material";
import React from "react";

const Label = ({ id = "", children, isFormLabel = false, className = "" }) => {
  const Element = isFormLabel ? FormLabel : InputLabel;

  return (
    <Element className={className} color="primary" id="id">
      {children}
    </Element>
  );
};

export default Label;
