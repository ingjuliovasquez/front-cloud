import React from "react";
import { Controller } from "react-hook-form";

const ControllerInput = ({className = 'block w-full', name, control, label, type = 'text', rules = {} }) => {
  return (
      <Controller
        name={name}
        control={control}
        rules={rules}
        render={({ field: { onChange } }) => (
          <input
            id={`input-${name}`}
            onChange={onChange}
            type={type}
            placeholder={label}
            className={`mb-3 p-3 border-gray-200 h-11 rounded-lg border-2 focus:ring-0 focus-visible:outline-none ${className}`}
          />
        )}
      />
  );
};

export default ControllerInput;
