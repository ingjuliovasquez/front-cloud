import React from "react";
import { FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import { Controller } from "react-hook-form";
import Label from './Label'

const RadioGroupController = ({
  name,
  control,
  label,
  className = "mb-3",
  flexDirection = 'flex-col',
  options = [],
  optionId = "id",
  optionName = "name",
  optionValue = null,
  isString = false,
  isHorizontal = false,
  ...props
}) => {
  return <Controller
    name={name}
    control={control}
    render = {({field: {onChange, value}, fieldState: {error}}) => (
        <FormControl>
            <div className={`flex ${flexDirection} ${className}`}>
                <Label isFormLabel id={`radriogroup-label-${name}`}>
                    {label}
                </Label>
                <RadioGroup
                    row={!!isHorizontal}
                    color='primary'
                    aria-labelledby={`radriogroup-label-${name}`}
                    name={name}
                    onChange={(_e, valueChange) => {
                        if (valueChange === 'true' || valueChange === 'false') {
                            onChange(valueChange === 'true')
                        } else {
                            onChange(isString ? valueChange : parseInt(valueChange, 10))
                        }
                    }}
                    value={value}
                    {...props}
                >
                    {
                        options.map((item) => (
                            <FormControlLabel 
                                key={`radioOption-${name}-${item[optionId]}`}
                                value={optionValue ? item[optionValue] : item[optionId]}
                                control={<Radio color="primary" size="small" />}
                                label={item[optionName]}
                            />
                        ))
                    }
                </RadioGroup>
            </div>
        </FormControl>
    )}
  />;
};

export default RadioGroupController;
