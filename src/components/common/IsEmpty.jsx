import React from "react";
import CloudOffIcon from "@mui/icons-material/CloudOff";

const IsEmpty = ({ message, size = "30px", Icon = CloudOffIcon }) => (
  <div className="w-1/2 container mx-auto rounded-lg bg-white text-black p-5 mt-5 flex flex-col justify-center items-center">
    <Icon sx={{ fontSize: size, color: 'rgb(37 99 235 / 1)' }} />
    <p className="font-semibold mt-5 text-center">{message}</p>
  </div>
);

export default IsEmpty;
