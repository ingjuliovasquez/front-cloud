export const updateRow = (array, row, optionId = "id") => {
  const newArray = [...array];
  return newArray.map(item => {
    if (row[optionId] === item[optionId]) return row
    return item
  })
};
